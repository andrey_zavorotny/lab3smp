import requests

from flask import Blueprint, render_template, request, Response, redirect, url_for, current_app
from flask_login import login_required

config_editor = Blueprint('config_editor', __name__, template_folder='templates', static_folder='static')


@config_editor.route('/config-editor')
@login_required
def render_config_editor():
    api_url = current_app.configuration['api_url']
    projects = requests.get(url=f'{api_url}/configs').json()
    configs = list()
    project = request.args.get('project')
    if project:
        configs = requests.get(url=f'{api_url}/configs/{project}').json()

    return render_template('config-editor.html', projects=projects, configs=configs)


@config_editor.route('/config-editor/save', methods=["POST"])
@login_required
def save():
    api_url = current_app.configuration['api_url']
    project = request.form.get('project')
    if project:
        requests.post(url=f'{api_url}/configs/{project}', json=dict())
        return redirect(url_for('config_editor.render_config_editor'))
    else:
        r = request.json
        requests.post(url=f'{api_url}/configs/{r["project"]}', json=r['config'])
    return redirect(url_for('config_editor.render_config_editor'))


@config_editor.route('/config-editor/activate')
@login_required
def activate():
    return Response(status=200)
