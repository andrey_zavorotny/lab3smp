import flask
import requests
from flask import Blueprint, render_template, redirect, url_for, current_app, request
from flask_login import login_required, current_user, logout_user, login_user

from models import User
from models.user import db

login = Blueprint('login', __name__, template_folder='templates', static_folder='static')


@login.route('/login', methods=['GET', 'POST'])
def login_user_():
    if flask.request.method == 'GET':
        return render_template('login.html')

    if not current_app.recaptcha.verify():
        return render_template('login.html', error_message='Подтвердите, что Вы не робот',
                               title='Login')

    user_login = request.form.get('login')
    user_password = request.form.get('password')

    try:
        r = requests.post(
            url=current_app.configuration['api_url'] + '/login',
            json={
                'username': user_login,
                'password': user_password
            }
        )
        r.raise_for_status()
    except requests.exceptions.HTTPError as e:
        return render_template('login.html', error_message='Неверный логин или пароль',
                               title='Login')
    except requests.exceptions.RequestException as e:
        raise e

    user = User.query.get(user_login)

    if user:
        user.authenticated = True
        user.token = r.json()
    else:
        user = User(
            login=user_login,
            password=user_password,
            token=r.json(),
            authenticated=True
        )

    db.session.add(user)
    db.session.commit()
    login_user(user, remember=True)

    return redirect(url_for('config_editor.render_config_editor'))


@login.route('/logout')
@login_required
def logout():
    user = current_user

    r = requests.post(
        url=current_app.configuration['api_url'] + '/logout',
        headers={'Authorization': f'Bearer {user.token}'}
    )

    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    return redirect(url_for('.login_user_'))
