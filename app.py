import os

from flask import Flask, redirect
from flask_login import LoginManager
from flask_recaptcha import ReCaptcha

from models import User
from models.user import db
from resources import config_editor, login
from utils import AppConfig

path = os.path.dirname(os.path.abspath(__file__))

# Load config with all dispatchers
config = AppConfig(os.path.join(path, 'config', 'config.json'))

app = Flask(__name__)
db.init_app(app)

with app.app_context():
    # Setup app config
    app.configuration = config
    # Init SECRET_KEY
    app.config['SECRET_KEY'] = config['secret_key']
    # Setup recaptcha
    app.config.update(config['recaptcha'])
    app.recaptcha = ReCaptcha(app=app)
    db.create_all()

# link resources routing
app.register_blueprint(login)
app.register_blueprint(config_editor)

# Setup authorization
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login.render_login'


@login_manager.user_loader
def user_loader(user_login):
    return User.query.get(user_login)


@login_manager.unauthorized_handler
def unauthorized_handler():
    return redirect('/login')


if __name__ == '__main__':
    app.run()
