from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(db.Model):
    __tablename__ = 'user'

    login = db.Column(db.String, primary_key=True)
    password = db.Column(db.String)
    token = db.Column(db.String)
    authenticated = db.Column(db.Boolean, default=False)

    @staticmethod
    def is_active():
        return True

    def get_id(self):
        return self.login

    def is_authenticated(self):
        return self.authenticated

    @staticmethod
    def is_anonymous():
        return False
