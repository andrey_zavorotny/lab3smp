function setValues() {
    var docs = document.getElementsByTagName("select");
    for (var i = 0; i < docs.length; i++) {
        docs[i].addEventListener("change", checkValue);
    }
}

setValues();

function checkValue() {
    var options = this.value;
    if (options == "default") {
        this.style.color = '#C6C8CB';
    } else {
        this.style.color = '#222222';
    }
}
function clearInputFile() {
    document.getElementById('customFile').value = '';
    document.getElementById("upload-icon").innerHTML = "";
    document.getElementById('custom-label1').innerHTML = "Перетащите файл сюда";
    document.getElementById('custom-label2').innerHTML = "";
}
